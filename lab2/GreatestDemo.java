
public class GreatestDemo {


	public static void main(String[] args)
	{
		int deger1 = Integer.parseInt(args[0]);
		int deger2 = Integer.parseInt(args[1]);
		int deger3 = Integer.parseInt(args[2]);
		int result;

		boolean kosul = deger1 > deger2;
		result = kosul ? deger1 : deger2;
	
		int result2;
		boolean kosul1 = result > deger3;
		result2 = kosul1 ? result : deger3;
		
		System.out.println(result2);
	}

}
